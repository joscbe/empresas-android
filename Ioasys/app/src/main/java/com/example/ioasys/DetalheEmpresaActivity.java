package com.example.ioasys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetalheEmpresaActivity extends AppCompatActivity {
    private Toolbar tbEmpresa;
    private ImageView ivEmpresa;
    private TextView tvDetalhes;

    private String nome;
    private String imagem;
    private String descricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_empresa);

        tbEmpresa = findViewById(R.id.toolbar_detalhe_empresa);
        ivEmpresa = findViewById(R.id.iv_detalhe_empresa);
        tvDetalhes = findViewById(R.id.tv_detalhe_emoresa);

        Intent intent = getIntent();

        nome = intent.getStringExtra("nome");
        imagem = intent.getStringExtra("imagem");
        descricao = intent.getStringExtra("descricao");

        tbEmpresa.setTitle(nome);
        setSupportActionBar(tbEmpresa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvDetalhes.setText(descricao);
        Picasso.get()
                .load(imagem)
                .into(ivEmpresa);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }
}