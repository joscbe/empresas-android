package com.example.ioasys.interfaces;

import android.view.View;

public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
