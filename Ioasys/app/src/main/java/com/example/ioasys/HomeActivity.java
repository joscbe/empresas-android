package com.example.ioasys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ioasys.adapters.EmpresasAdapter;
import com.example.ioasys.config.RetrofitConfig;
import com.example.ioasys.domain.Empresas;
import com.example.ioasys.interfaces.RecyclerViewOnClickListenerHack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private List<Empresas> list;
    private TextView tvMessage;
    private TextView tvMessageVazio;
    private ProgressBar progressBar;

    private String token;
    private String client;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = findViewById(R.id.toolbar_home);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        tvMessage = findViewById(R.id.message_incial);
        tvMessageVazio = findViewById(R.id.tv_messagem_vazio);
        progressBar = findViewById(R.id.progress_home);
        progressBar.setVisibility(View.GONE);
        tvMessageVazio.setVisibility(View.GONE);

        Intent intent = getIntent();
        token = intent.getStringExtra("token");
        client = intent.getStringExtra("client");
        uid = intent.getStringExtra("uid");

        recyclerView = findViewById(R.id.rv_empresas);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        /*list = getEmpresas(10);
        EmpresasAdapter adapter = new EmpresasAdapter(this, list);
        adapter.setRecyclerViewOnClickListenerHack(this);
        recyclerView.setAdapter(adapter);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Acontece quando clica o botão de pesquisa
                tvMessage.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                tvMessageVazio.setVisibility(View.GONE);

                recyclerView.removeAllViews();

                String search;
                search = searchView.getQuery().toString();

                Call<ResponseBody> call = new RetrofitConfig()
                        .getApiService()
                        .getListEmpresas(search, token, client, uid);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String body = response.body().string();
                            Log.i("listEmpresas", body);

                            list = getListEmpresas(body);
                            EmpresasAdapter adapter = new EmpresasAdapter(getApplication(), list);
                            adapter.setRecyclerViewOnClickListenerHack(HomeActivity.this);
                            recyclerView.setAdapter(adapter);

                            if (list.isEmpty())
                                tvMessageVazio.setVisibility(View.VISIBLE);

                            progressBar.setVisibility(View.GONE);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Acontece enquanto digita
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void toast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    /**
     * private List<Empresas> getEmpresas(int quant){
     * List<Empresas> list = new ArrayList<>();
     * String[] image = new String[]{"nada"};
     * String[] nome = new String[]{"Empresa1", "Empresa2", "Empresa3"};
     * String[] tipo = new String[]{"Negocios", "Vendas", "Saude"};
     * String[] pais = new String[]{"Brasil", "Brasil", "EUA"};
     * <p>
     * for (int i=0;i<quant;i++){
     * Empresas empresas = new Empresas(nome[i%nome.length], tipo[i%tipo.length], pais[i%pais.length], image[i%image.length]);
     * list.add(empresas);
     * }
     * return list;
     * }
     */

    private List<Empresas> getListEmpresas(String body) {
        List<Empresas> list = new ArrayList<>();

        try {
            JSONObject objectArray = new JSONObject(body);
            JSONArray jsonArray = objectArray.getJSONArray("enterprises");
            JSONObject jsonObject;

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                String nome = jsonObject.getString("enterprise_name");
                String foto = jsonObject.getString("photo");
                String pais = jsonObject.getString("country");
                String descricao = jsonObject.getString("description");
                String tipo = jsonObject.getJSONObject("enterprise_type").getString("enterprise_type_name");

                Empresas empresas = new Empresas(nome, tipo, pais, foto, descricao);
                list.add(empresas);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void onClickListener(View view, int position) {
        //Nome, imagem, descricao
        Intent intent = new Intent(HomeActivity.this, DetalheEmpresaActivity.class);
        intent.putExtra("nome", list.get(position).getNome());
        intent.putExtra("imagem", list.get(position).getImage());
        intent.putExtra("descricao", list.get(position).getDescricao());

        startActivity(intent);
    }
}