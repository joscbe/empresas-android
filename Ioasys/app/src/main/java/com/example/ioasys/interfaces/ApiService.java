package com.example.ioasys.interfaces;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    Call<ResponseBody> loginUser(
            @Field("email") String email,
            @Field("password") String senha
    );

    @GET("enterprises?")
    Call<ResponseBody> getListEmpresas(
            @Query("name") String nome,
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid
            );
}
