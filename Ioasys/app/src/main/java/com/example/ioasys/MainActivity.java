package com.example.ioasys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ioasys.config.RetrofitConfig;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;

public class MainActivity extends AppCompatActivity {
    private Button btnLogin;
    private EditText edtEmail;
    private EditText edtSenha;
    private TextView tvMessageErro;

    private ConstraintLayout telaCarregando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = findViewById(R.id.btn_login);
        edtEmail = findViewById(R.id.edt_email);
        edtSenha = findViewById(R.id.edt_senha);
        tvMessageErro = findViewById(R.id.tv_message_error);

        tvMessageErro.setVisibility(View.GONE);

        telaCarregando = findViewById(R.id.tela_carregando);
        telaCarregando.setVisibility(View.GONE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtEmail.getText().toString();
                String senha = edtSenha.getText().toString();

                if (email.isEmpty()){
                    edtEmail.setError("Insira um email");
                    edtEmail.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    edtEmail.setError("Insira um email valido");
                    edtEmail.requestFocus();
                    return;
                }

                if (senha.isEmpty()){
                    edtSenha.setError("Insira a senha");
                    edtSenha.requestFocus();
                    return;
                }

                if (senha.length() < 6){
                    edtSenha.setError("Senha deve ser maior");
                    edtSenha.requestFocus();
                    return;
                }
                telaCarregando.setVisibility(View.VISIBLE);

                Call<ResponseBody> call = new RetrofitConfig()
                        .getApiService()
                        .loginUser(email, senha);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        //Em caso de sucesso
                        try {
                            if (response.isSuccessful()){
                                Log.i("login", "entrou");
                                String s = response.body().string();

                                String header = response.headers().toString();

                                String token = response.headers().get("access-token");
                                String client = response.headers().get("client");
                                String uid = response.headers().get("uid");

                                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                intent.putExtra("token", token);
                                intent.putExtra("client", client);
                                intent.putExtra("uid", uid);
                                startActivity(intent);

                                telaCarregando.setVisibility(View.GONE);
                            } else {
                                telaCarregando.setVisibility(View.GONE);
                                Log.e("login", "deu erro");
                                String bodyError = response.errorBody().string();
                                tvMessageErro.setText("Email ou senha invalido.");
                                tvMessageErro.setVisibility(View.VISIBLE);
                                //Toast.makeText(getApplication(), bodyError, Toast.LENGTH_LONG).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //Em caso de falha
                        Toast.makeText(getApplication(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}